<?php

use Illuminate\Database\Seeder;

use App\Sell;
use App\Client;
use App\Product;

use Illuminate\Support\Facades\DB;

class SellTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = Client::find(1);
        $product_1 = Product::find(1);
        $product_2 = Product::find(2);
        $total=0;

        //sell 1
            $sell = Sell::create([
                'client_id'      => $client->id,
            ]);

            $sub_total = (float)($product_1->price * 1);
            $total += $sub_total ;
            $detail = DB::table('product_sell')->insert(
                [
                    'sell_id'        => $sell->id,
                    'product_id'     => $product_1->id,
                    'quantity'       => 1,
                    'sub_total'      => $sub_total
                ]
            );

            $sell->total = $sub_total;
            $sell->save();
        //sell 1

        //sell 2
            $sub_total = (float)($product_2->price * 2);
            $total += $sub_total ;
            $detail = DB::table('product_sell')->insert(
                [
                    'sell_id'        => $sell->id,
                    'product_id'     => $product_2->id,
                    'quantity'       => 2,
                    'sub_total'      => $sub_total
                ]
            );

            $sell->total = $total;
            $sell->save();
        //sell 2



    }
}
