<?php

use Illuminate\Database\Seeder;

use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'      => 'Bebida Cocacola 1LT',
                'price'     => 500
            ],
            [
                'name'      => 'Bebida Sprite 1LT',
                'price'     => 500
            ]
        ];

        Product::insert($data);
    }
}
