@extends('layouts.web')

@section('content')

<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Last Name</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($clients as $item)
            <tr>
                <td>{{ $item->name}}</td>
                <td>{{ $item->last_name}}</td>
            </tr>
        @endforeach
    </tbody>
</table>


@endsection
