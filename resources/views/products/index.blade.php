@extends('layouts.web')

@section('content')

<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Price</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($products as $item)
            <tr>
                <td>{{ $item->name}}</td>
                <td>{{ $item->price}}</td>
            </tr>
        @endforeach
    </tbody>
</table>


@endsection
