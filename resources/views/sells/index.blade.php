@extends('layouts.web')

@section('content')

<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Last Name</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($sells as $item)
            <tr>
                <td>{{ $item->client->name}}</td>
                <td>{{ $item->client->last_name}}</td>
            </tr>
        @endforeach
    </tbody>
</table>


@endsection
