<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Client;
use App\Product;

class Sell extends Model
{
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)
        ->withPivot('quantity', 'sub_total');
    }
}
