<?php

namespace App\Http\Controllers;

use App\Sell;
use Illuminate\Http\Request;

use App\Client;
use App\Product;

class SellController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sell = Sell::where('client_id', 1)->first();


        $products = [];
        foreach($sell->products as $item)
        {
            array_push($products, [
                'quantity'  => $item->pivot->quantity,
                'product'   => $item->name,
                'price'     => $item->price,
                'sub_total' => $item->pivot->sub_total,
            ]);
        }

        $sell = Sell::where('client_id', 1)->first();
        $json = [
            [
                'id'        => $sell->id,
                'client'    => [
                    'name'     => $sell->client->name,
                    'last_name'     => $sell->client->last_name
                ],
                'detail'    => [$products],
                'total'     => $sell->total
            ]
        ];

        return response()->json($json);
    }

    public function sell()
    {
        $sells = Sell::where('total', '>', 5)->get();

        $data = [
            'title'     => 'CLientes con ventas mayor a 5',
            'sells'  => $sells
        ];

        return view ('sells.index',  $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sell  $sell
     * @return \Illuminate\Http\Response
     */
    public function show(Sell $sell)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sell  $sell
     * @return \Illuminate\Http\Response
     */
    public function edit(Sell $sell)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sell  $sell
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sell $sell)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sell  $sell
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sell $sell)
    {
        //
    }
}
